package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/users/")
public class UserController {

    @Autowired
    UserService userService;


    @PostMapping
        public ModelAndView newUser(@RequestBody User newuser){

        ModelAndView modelAndView=new ModelAndView("usercreated");
        userService.save(newuser);
        modelAndView.addObject("user",newuser);
        return modelAndView;
    }


    @GetMapping("{id}")
        public ModelAndView getUser(@PathVariable Long id){
        ModelAndView modelAndView=new ModelAndView("userdetails");
        User user= userService.get(id);
        modelAndView.addObject("user",user);
        return modelAndView;
    }

    @PutMapping()
    public ModelAndView updateContact(@RequestBody User user){
        ModelAndView modelAndView=new ModelAndView("userupdated");

        User updateuser= userService.get(user.getId());
        updateuser.setContact(user.getContact());

        userService.update(updateuser);

        modelAndView.addObject("user",user);
        return modelAndView;
    }

    @DeleteMapping("{id}")
    public ModelAndView deleteUser(@PathVariable Long id){
        ModelAndView modelAndView=new ModelAndView("userdeleted");


        User tobeDeleted= userService.get(id);
        userService.delete(id);

        modelAndView.addObject("user",tobeDeleted);
        return modelAndView;
    }


    @RequestMapping("/")
    public ModelAndView listAll(){
        ModelAndView modelAndView=new ModelAndView("message");

        List<User> users= userService.getAll();

       StringBuffer buffer=new StringBuffer();
       Iterator<User> iterator=users.iterator();
        while(iterator.hasNext()){
            buffer.append(iterator.next()+"\n");
        }


        modelAndView.addObject("msg",buffer);
        return modelAndView;
    }

}
