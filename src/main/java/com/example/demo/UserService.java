package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


@Service
public class UserService implements Dao<User> {
    @Autowired
    UserRepository repository;

    @Override
    public User get(Long id) {
        Optional<User> users=repository.findById(id);
        User user=users.get();
        return user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> listUsers=new ArrayList<>();

        Iterator<User> iterator=repository.findAll().iterator();
        while(iterator.hasNext()){
            listUsers.add(iterator.next());
        }

        return listUsers;
    }

    @Override
    public User save(User user) {
        return repository.save(user);

    }

    @Override
    public void update(User user) {
        Optional<User> users=repository.findById(user.getId());
        User newUser=new User(users.get().getName(),user.getId(),user.getContact());
        repository.delete(users.get());
        repository.save(newUser);

    }

    @Override
    public void delete(Long id) {
        Optional<User> users=repository.findById(id);
        User tobeDeleted=users.get();
        repository.delete(tobeDeleted);
    }
}
