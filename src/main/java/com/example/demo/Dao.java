package com.example.demo;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    T get(Long id);
    List<T> getAll();
    T save(T t);
    void update(T t);
    void delete(Long id);
}
