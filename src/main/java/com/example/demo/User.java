package com.example.demo;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="users")
public class User implements Serializable {

    @Id
    @Column(name = "id",insertable = false,updatable = false)
    private Long id;


    @Column(name = "name",insertable = true,updatable = true)
    private String name;

    @Column(name = "contact",insertable = true,updatable = true)
    private String contact;




    public User(){}
    public User(String name, Long id,String contact) {
        this.name = name;
        this.id = id;
        this.contact=contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return name+" "+id+" "+contact;
    }
}
